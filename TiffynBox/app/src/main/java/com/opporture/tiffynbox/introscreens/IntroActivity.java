package com.opporture.tiffynbox.introscreens;


import androidx.core.content.ContextCompat;
import android.os.Bundle;

import com.github.appintro.AppIntro;
import com.github.appintro.AppIntroFragment;
import com.opporture.tiffynbox.R;

public class IntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        addSlide(AppIntroFragment.newInstance("C++", "C++ Self Paced Course",
                R.mipmap.ic_launcher, ContextCompat.getColor(getApplicationContext(), R.color.purple_200)));

        // below line is for creating second slide.
        addSlide(AppIntroFragment.newInstance("DSA", "Data Structures and Algorithms", R.mipmap.ic_launcher,
                ContextCompat.getColor(getApplicationContext(), R.color.purple_200)));

        // below line is use to create third slide.
        addSlide(AppIntroFragment.newInstance("Java", "Java Self Paced Course", R.mipmap.ic_launcher,
                ContextCompat.getColor(getApplicationContext(), R.color.purple_200)));
    }
}