package com.opporture.tiffynbox.services;


import com.opporture.tiffynbox.modal.CategoryDao;
import com.opporture.tiffynbox.modal.LoginDao;
import com.opporture.tiffynbox.modal.PinCreationDao;
import com.opporture.tiffynbox.modal.ProfileDao;
import com.opporture.tiffynbox.modal.ProfileData;
import com.opporture.tiffynbox.modal.RegistraionDao;
import com.opporture.tiffynbox.modal.SubItemDao;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegistrationApi {

    @FormUrlEncoded
    @POST("User_registration/registeration")
    Call<RegistraionDao> toVerifyMobileNumber(
            @Field("mobile") String Username);

    @FormUrlEncoded
    @POST("User_registration/user_pin")
    Call<PinCreationDao> toCreatePin(
            @Field("username") String Username,
            @Field("pin") String Pin,
            @Field("confirm_pin") String ConfirmPin);

    @FormUrlEncoded
    @POST("User_registration/login")
    Call<LoginDao> toUserLogin(
            @Field("mobile") String Username,
            @Field("pin") String Pin);

    @FormUrlEncoded
    @POST("Category/getCategories")
    Call<CategoryDao> toGetCategories(
            @Field("mobile") String Username,
            @Field("pin") String Pin);



    @FormUrlEncoded
    @POST("Category/getitemsbyid")
    Call<SubItemDao> toGetSubItems(
            @Field("cat_id") String CategoryID);


    @FormUrlEncoded
    @POST("Profile_Update/adduserdetails")
    Call<ProfileDao> toUpdateUserDetails(
            @Field("user_id") String UserID,
            @Field("name") String UserName,
            @Field("email") String Email);
}
