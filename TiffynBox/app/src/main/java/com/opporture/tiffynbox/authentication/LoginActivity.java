package com.opporture.tiffynbox.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.opporture.tiffynbox.R;
import com.opporture.tiffynbox.modal.RegistraionDao;
import com.opporture.tiffynbox.services.RegistrationApi;
import com.opporture.tiffynbox.services.ServiceGenerator;
import com.opporture.tiffynbox.utils.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Button letsButton;
    TextInputLayout inputLayout;
    EditText mobilenumber;
    ProgressBar progressBar;
    String Mobile;
    RegistraionDao registraionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        initViews();
        iniListeners();
    }

    private void iniListeners() {
        letsButton.setOnClickListener(this);
    }

    private void initViews() {
        mobilenumber = findViewById(R.id.username_edtv);
        letsButton = findViewById(R.id.letsButton);
        progressBar = findViewById(R.id.progressbar);
        inputLayout = findViewById(R.id.usernamelayout);
    }

    @Override
    public void onClick(View view) {
        if (view == letsButton) {
            InputMethodManager hide = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            hide.hideSoftInputFromWindow(letsButton.getWindowToken(),
                    InputMethodManager.RESULT_UNCHANGED_SHOWN);
            validateForm();
        }
    }

    private void validateForm() {
        Mobile = mobilenumber.getText().toString().trim();
        if (!Util.isValidPhoneNumber(Mobile)) {
            inputLayout.setError("Enter Valid Mobile Number");
        } else {
            if (Util.isNetworkAvailable(this)) {
                progressBar.setVisibility(View.VISIBLE);
                sendRequestToServer();
            } else {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(letsButton, "Check Your Connection", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private void sendRequestToServer() {
        RegistrationApi api = ServiceGenerator.createService(RegistrationApi.class);
        Call<RegistraionDao> daoCall = api.toVerifyMobileNumber(Mobile);
        daoCall.enqueue(new Callback<RegistraionDao>() {
            @Override
            public void onResponse(Call<RegistraionDao> call, Response<RegistraionDao> response) {
                if (response.isSuccessful()) {
                    registraionDao = response.body();
                    if (registraionDao.getUserData().getSuccess().equalsIgnoreCase("Created - User information & OTP Sent Successfully !")) {
                        progressBar.setVisibility(View.GONE);
                        startActivity(new Intent(getApplicationContext(), LoginPinActivity.class)
                                .putExtra("mobile", "" + Mobile));
                    }
                    else {
                        if(registraionDao.getUserData().getSuccess().equalsIgnoreCase("User already registered. Login to your account"))
                            progressBar.setVisibility(View.GONE);
                        startActivity(new Intent(getApplicationContext(),LoginwithActivity.class)
                                .putExtra("mobile", "" + Mobile));
                    }
                }
            }

            @Override
            public void onFailure(Call<RegistraionDao> call, Throwable t) {
                try {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(letsButton, "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();
                    Log.d("message", "" + t.getMessage());
                    System.out.println("onFAilureExecute" + t.getMessage());
                    if (t != null)
                        t.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }

            }
        });
    }

}