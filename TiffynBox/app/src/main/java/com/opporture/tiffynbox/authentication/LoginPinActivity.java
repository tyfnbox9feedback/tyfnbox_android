package com.opporture.tiffynbox.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.opporture.tiffynbox.R;
import com.opporture.tiffynbox.dashboard.DashboardActivity;
import com.opporture.tiffynbox.modal.PinCreationDao;
import com.opporture.tiffynbox.services.RegistrationApi;
import com.opporture.tiffynbox.services.ServiceGenerator;
import com.opporture.tiffynbox.utils.Constants;
import com.opporture.tiffynbox.utils.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPinActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    Button pinButton;
    EditText setpinOne, setpinTwo, setpinThree, setpinFour, csetpinOne, csetpinTwo, csetpinThree, csetpinFour;
    String UserID, Mobile, Pinone, Pintwo, PinThree, PinFour, CPinone, CPintwo, CPinThree, CPinFour, Pin, ConfirmPin;
    PinCreationDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_pin);
        Intent intent = getIntent();
        Mobile = intent.getStringExtra("mobile");
        initViews();
        initListeners();
        setWatcherListener();
    }

    private void initListeners() {
        pinButton.setOnClickListener(this);
    }

    private void initViews() {
        setpinOne = findViewById(R.id.etPin1);
        setpinTwo = findViewById(R.id.etPin2);
        setpinThree = findViewById(R.id.etPin3);
        setpinFour = findViewById(R.id.etPin4);

        csetpinOne = findViewById(R.id.cetPin1);
        csetpinTwo = findViewById(R.id.cetPin2);
        csetpinThree = findViewById(R.id.cetPin3);
        csetpinFour = findViewById(R.id.cetPin4);

        pinButton = findViewById(R.id.pinButton);

    }

    @Override
    public void onClick(View view) {
        if (view == pinButton) {
            InputMethodManager hide = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            hide.hideSoftInputFromWindow(pinButton.getWindowToken(),
                    InputMethodManager.RESULT_UNCHANGED_SHOWN);
            validateForm();
        }
    }

    private void validateForm() {
        Pinone = setpinOne.getText().toString().trim();
        Pintwo = setpinTwo.getText().toString().trim();
        PinThree = setpinThree.getText().toString().trim();
        PinFour = setpinFour.getText().toString().trim();

        CPinone = csetpinOne.getText().toString().trim();
        CPintwo = csetpinTwo.getText().toString().trim();
        CPinThree = csetpinThree.getText().toString().trim();
        CPinFour = csetpinFour.getText().toString().trim();

        Pin = Pinone + "" + Pintwo + "" + PinThree + "" + PinFour;
        ConfirmPin = CPinone + "" + CPintwo + "" + CPinThree + "" + CPinFour;
        Log.e("pin", "" + Pin);
        Log.e("cpin", "" + ConfirmPin);
        Toast.makeText(getApplicationContext(), "" + Pin + ConfirmPin, Toast.LENGTH_SHORT).show();
        if (!Pin.equals(ConfirmPin)) {
            // Toast.makeText(this, "Pin doesn't Match", Toast.LENGTH_SHORT).show();
            return;
        } else {
            senDataToServer();
        }

    }

    private void senDataToServer() {
        RegistrationApi api = ServiceGenerator.createService(RegistrationApi.class);
        Call<PinCreationDao> call = api.toCreatePin(Mobile, Pin, ConfirmPin);
        call.enqueue(new Callback<PinCreationDao>() {
            @Override
            public void onResponse(Call<PinCreationDao> call, Response<PinCreationDao> response) {
                if (response.isSuccessful()) {
                    dao = response.body();
                    if (dao.getResponse().equalsIgnoreCase("Success")) {
                        Util.saveBooleanInSP(LoginPinActivity.this, Constants.IS_LOGIN, true);
                        if (UserID != null) {
                            Util.saveStringInSP(LoginPinActivity.this, "user_id", dao.getAddData().getData().getUserId());
                        }
                        startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                    }
                } else {
                    Snackbar.make(pinButton, "" + dao.getResponse(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PinCreationDao> call, Throwable t) {
                try {
                    Snackbar.make(pinButton, "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();
                    Log.d("message", "" + t.getMessage());
                    System.out.println("onFAilureExecute" + t.getMessage());
                    if (t != null)
                        t.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }

            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        switchToNextBox();

    }

    // This method is used to switch to next opt box
    public void switchToNextBox() {
        if (setpinOne.getText().toString().length() == 1 && setpinTwo.getText().toString().length() != 1 && setpinThree.getText().toString().length() != 1 && setpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                   setpinTwo.requestFocus();
                }
            }, 50);
        } else if (setpinOne.getText().toString().length() == 1 && setpinTwo.getText().toString().length() == 1 && setpinThree.getText().toString().length() != 1 && setpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                   setpinThree.requestFocus();
                }
            }, 50);
        } else if (setpinOne.getText().toString().length() == 1 && setpinTwo.getText().toString().length() == 1 && setpinThree.getText().toString().length() == 1 && setpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setpinFour.requestFocus();
                }
            }, 50);
        } else if (csetpinOne.getText().toString().length() == 1 && csetpinTwo.getText().toString().length() != 1 && csetpinThree.getText().toString().length() != 1 && csetpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    csetpinTwo.requestFocus();
                }
            }, 50);
        } else if (csetpinOne.getText().toString().length() == 1 && csetpinTwo.getText().toString().length() == 1 && csetpinThree.getText().toString().length() != 1 && csetpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    csetpinThree.requestFocus();
                }
            }, 50);
        } else if (csetpinOne.getText().toString().length() == 1 && csetpinTwo.getText().toString().length() == 1 && csetpinThree.getText().toString().length() == 1 && csetpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    csetpinFour.requestFocus();
                }
            }, 50);
        }
    }

    // This is used to set TextWatcher Listener
    public void setWatcherListener() {
        setpinOne.addTextChangedListener(this);
        setpinTwo.addTextChangedListener(this);
        setpinThree.addTextChangedListener(this);
        setpinFour.addTextChangedListener(this);

       csetpinOne.addTextChangedListener(this);
        csetpinTwo.addTextChangedListener(this);
        csetpinThree.addTextChangedListener(this);
       csetpinFour.addTextChangedListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        handleBackPressFocus();
        return super.onKeyDown(keyCode, event);
    }
    // handle focus when user press back key of soft keyboard
    private void handleBackPressFocus() {
        if (setpinFour.hasFocus()) {
            setpinThree.requestFocus();
            setpinThree.setSelection(setpinThree.getText().length());
        } else if (setpinThree.hasFocus()) {
            setpinTwo.requestFocus();
            setpinTwo.setSelection(setpinTwo.getText().length());
        } else if (setpinTwo.hasFocus()) {
            setpinOne.requestFocus();
            setpinOne.setSelection(setpinOne.getText().length());
        }

        if (csetpinFour.hasFocus()) {
            csetpinThree.requestFocus();
           csetpinThree.setSelection(csetpinThree.getText().length());
        } else if (csetpinThree.hasFocus()) {
            csetpinTwo.requestFocus();
            csetpinTwo.setSelection(csetpinTwo.getText().length());
        } else if (csetpinTwo.hasFocus()) {
            csetpinOne.requestFocus();
            csetpinOne.setSelection(csetpinOne.getText().length());
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}