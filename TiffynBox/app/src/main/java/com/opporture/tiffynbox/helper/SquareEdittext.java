package com.opporture.tiffynbox.helper;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

public class SquareEdittext extends AppCompatEditText {
    public SquareEdittext(Context context) {
        super(context);
    }

    public SquareEdittext(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareEdittext(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }
}