package com.opporture.tiffynbox.fragments;


import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.opporture.tiffynbox.R;
import com.opporture.tiffynbox.modal.CategoryDao;
import com.opporture.tiffynbox.modal.CategoryListDatum;
import com.opporture.tiffynbox.modal.SubItemDao;
import com.opporture.tiffynbox.modal.SubListDatum;
import com.opporture.tiffynbox.services.RegistrationApi;
import com.opporture.tiffynbox.services.ServiceGenerator;
import com.opporture.tiffynbox.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    View view;
    ListView listView;
    ProgressBar progressBar;
    String Category, Username, Pin;
    RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    SubItemAdapter adapter;
    CatgeoryAdapter catgeoryAdapter;
    SubItemDao dao;
    CategoryDao categoryDao;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews();
        Category="15";
        setupCategories();
        getSubItems();
        return view;
    }

    private void setupCategories() {
        if (Util.isNetworkAvailable(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            sendRequesttoServer();
        } else {
            progressBar.setVisibility(View.GONE);
            Snackbar.make(progressBar, "check connection", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void sendRequesttoServer() {
        RegistrationApi api= ServiceGenerator.createService(RegistrationApi.class);
        Call<CategoryDao> call=api.toGetCategories(Username,Pin);
        call.enqueue(new Callback<CategoryDao>() {
            @Override
            public void onResponse(Call<CategoryDao> call, Response<CategoryDao> response) {
                if(response.isSuccessful()){
                    categoryDao=response.body();
                    if(categoryDao.getResponse().equalsIgnoreCase("Success")){
                        progressBar.setVisibility(View.GONE);
                        List<CategoryListDatum> list=categoryDao.getData().getData();
                        catgeoryAdapter = new CatgeoryAdapter(getContext(), list);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setAdapter(catgeoryAdapter);
                    }
                    else {
                        progressBar.setVisibility(View.GONE);
                        Snackbar.make(listView, "No data found", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(listView,"no data found", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategoryDao> call, Throwable t) {
                try {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(listView,"no data found", Snackbar.LENGTH_SHORT).show();
                    Log.d("message", "" + t.getMessage());
                    System.out.println("onFAilureExecute" + t.getMessage());
                    if (t != null)
                        t.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }

            }
        });
    }

    private void initViews() {
            listView = view.findViewById(R.id.listview);
            progressBar = view.findViewById(R.id.progressbar);
            recyclerView = view.findViewById(R.id.categories);
            linearLayoutManager = new LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false);


    }

    private void getSubItems() {
        if (Util.isNetworkAvailable(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            fetchDataFromServer();
        } else {
            progressBar.setVisibility(View.GONE);
            Snackbar.make(progressBar, "check connection", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void fetchDataFromServer() {
        RegistrationApi api = ServiceGenerator.createService(RegistrationApi.class);
        Call<SubItemDao> call = api.toGetSubItems(Category);
        call.enqueue(new Callback<SubItemDao>() {
            @Override
            public void onResponse(Call<SubItemDao> call, Response<SubItemDao> response) {
                if (response.isSuccessful()) {
                    dao = response.body();
                    if (dao.getResponse().equalsIgnoreCase("Success")) {
                        progressBar.setVisibility(View.GONE);
                        List<SubListDatum> list = dao.getData().getData();
                        adapter = new SubItemAdapter(getContext(), list);
                        listView.setAdapter(adapter);
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Snackbar.make(listView, "No data found", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(listView, "no data found", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SubItemDao> call, Throwable t) {
                try {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(listView, "no data found", Snackbar.LENGTH_SHORT).show();
                    Log.d("message", "" + t.getMessage());
                    System.out.println("onFAilureExecute" + t.getMessage());
                    if (t != null)
                        t.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }

            }
        });

    }
private class SubItemAdapter extends BaseAdapter {
    Context context;
    private List<SubListDatum> members;

    public SubItemAdapter(Context applicationContext, List<SubListDatum> members) {
        this.context = applicationContext;
        this.members = members;


    }

    @Override
    public int getCount() {
        return members.size();
    }

    @Override
    public Object getItem(int i) {
        return members.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = null;
        LayoutInflater layoutInflater = null;
        TextView fname, dobTv, locationTv, location, voterscount;
        final ImageView profile, imageview;
        RelativeLayout itemrlyt, smsrlyt;
        LinearLayout linearLayout, attachlayout;
        RatingBar ratingBar;
        double rating = 0;
        final String Fname, Voterid, Location, Designation, Voterscount, Dob, Userid, PhoneNumber, Photo;
        layoutInflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.list_subitems, null);
        itemrlyt = view.findViewById(R.id.rlyt);
        profile = view.findViewById(R.id.image_iv);
        fname = view.findViewById(R.id.name_tv);
        dobTv = view.findViewById(R.id.price_tv);
        ratingBar = view.findViewById(R.id.rating);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            profile.setClipToOutline(true);
        }

        SubListDatum membersdata = members.get(i);
        Fname = membersdata.getItemName();
        Voterid = membersdata.getItemDescription();
        Designation = membersdata.getItemRating();
        Userid = membersdata.getId();
        PhoneNumber = membersdata.getItemPrice();
        Photo = membersdata.getImage();
        Log.d("attach", Photo);


        if (Photo.isEmpty()) {
        } else {
            profile.setVisibility(View.VISIBLE);
            //Picasso.with(getApplicationContext()).load(Photo).into(imageview);
            //  Picasso.with(getApplicationContext()).load(Photo).transform(new RoundCorners(this.context)).into(imageview);
            Picasso.with(getContext()).load(Photo).into(profile);
        }

        fname.setText(Fname);
        dobTv.setText(PhoneNumber);
        if (Designation.equalsIgnoreCase("No Ratings Found")) {
            rating = 3.5;
            ratingBar.setRating((float) rating);
        } else {
            ratingBar.setRating((float) rating);
        }

        // location.setText(Location);


        return view;
    }


}
}