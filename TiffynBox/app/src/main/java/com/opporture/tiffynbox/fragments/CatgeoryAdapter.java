package com.opporture.tiffynbox.fragments;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.opporture.tiffynbox.R;
import com.opporture.tiffynbox.modal.CategoryListDatum;
import com.opporture.tiffynbox.modal.SubItemDao;
import com.opporture.tiffynbox.modal.SubListDatum;
import com.opporture.tiffynbox.services.RegistrationApi;
import com.opporture.tiffynbox.services.ServiceGenerator;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatgeoryAdapter extends RecyclerView.Adapter<CatgeoryAdapter.CustomViewHolder> {
    private Context context;
    private List<CategoryListDatum> categoryListData;
    CategoryListDatum listDatum;
    String Category;
    SubItemDao dao;
    public CatgeoryAdapter(Context context, List<CategoryListDatum> list) {
        this.context=context;
        this.categoryListData=list;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false); //TODO your xml
        return new CustomViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder  customViewHolder, final int pos) {
        listDatum = categoryListData.get(pos);
        String Photo = listDatum.getCategoryName();
         Category=listDatum.getId();
        customViewHolder.textView.setText(Photo);
        Log.d("image", "" + Photo);

        }


    @Override
    public int getItemCount() {
            return categoryListData == null ? 0 : categoryListData.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon;
        private TextView textView;
        private CardView cardView;
        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView=itemView.findViewById(R.id.card_view);
            textView = itemView.findViewById(R.id.category);
        }
    }
}

