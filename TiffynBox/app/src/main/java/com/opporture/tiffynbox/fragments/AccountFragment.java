package com.opporture.tiffynbox.fragments;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.opporture.tiffynbox.R;
import com.opporture.tiffynbox.modal.ProfileDao;
import com.opporture.tiffynbox.services.RegistrationApi;
import com.opporture.tiffynbox.services.ServiceGenerator;
import com.opporture.tiffynbox.utils.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountFragment extends Fragment implements View.OnClickListener {
    View view;
    String Mobile,ProfileName,Email,UserId;
    EditText mobileEdtv,profileEdtv,emailEdtv;
    TextInputLayout userlyt,mobilelyt,emaillyt;
    ProgressBar progressBar;
    Button button;
    ProfileDao dao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);
        UserId = Util.getStringFromSP(getContext(), "user_id");
        Mobile = Util.getStringFromSP(getContext(), "mobile");
        Log.e("params",""+UserId+Mobile);
        initviews();
        initListeners();
       // getProfileData();
        return view;
    }

    private void initListeners() {
        button.setOnClickListener(this);
    }

    private void getProfileData() {
            if(Util.isNetworkAvailable(getContext())){
                progressBar.setVisibility(View.VISIBLE);
                fetchDataFromServer();
            }else{
                progressBar.setVisibility(View.GONE);
                Snackbar.make(progressBar, "check connection", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void fetchDataFromServer() {
        RegistrationApi  api= ServiceGenerator.createService(RegistrationApi.class);
        Call<ProfileDao>call=api.toUpdateUserDetails(UserId,ProfileName,Email);
        call.enqueue(new Callback<ProfileDao>() {
            @Override
            public void onResponse(Call<ProfileDao> call, Response<ProfileDao> response) {
                if (response.isSuccessful()) {
                    dao = response.body();
                    if (dao.getResponse().equalsIgnoreCase("Success")) {
                        progressBar.setVisibility(View.GONE);
                        ProfileName = dao.getData().getData().getName();
                        Email = dao.getData().getData().getEmail();
                        profileEdtv.setText(ProfileName);
                        emailEdtv.setText(Email);
                        mobileEdtv.setText(Mobile);
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Snackbar.make(button, "No data found"+dao.getResponse(), Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(button, "no data found"+response.body(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileDao> call, Throwable t) {
                try {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(button,"no data found", Snackbar.LENGTH_SHORT).show();
                    Log.d("message", "" + t.getMessage());
                    System.out.println("onFAilureExecute" + t.getMessage());
                    if (t != null)
                        t.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }

            }
        });
    }

    private void initviews() {
        userlyt=view.findViewById(R.id.usernamelayout);
        profileEdtv=view.findViewById(R.id.username_edtv);
        mobilelyt=view.findViewById(R.id.phonelayout);
        mobileEdtv=view.findViewById(R.id.phone_edtv);
        emaillyt=view.findViewById(R.id.emaillayout);
        emailEdtv=view.findViewById(R.id.email_edtv);
        button=view.findViewById(R.id.updatedButton);
        progressBar=view.findViewById(R.id.progressbar);
    }

    @Override
    public void onClick(View view) {
        if(view==button){
            InputMethodManager hide = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            hide.hideSoftInputFromWindow(button.getWindowToken(),
                    InputMethodManager.RESULT_UNCHANGED_SHOWN);
            validateForm();
        }
    }

    private void validateForm() {
        ProfileName = profileEdtv.getText().toString().trim();
        Email = emailEdtv.getText().toString().trim();
        if (ProfileName.isEmpty()) {
            userlyt.setError("must enter");
        } else {
            if (Email.isEmpty()) {
                emaillyt.setError("must enter");
            } else {
                if (Util.isNetworkAvailable(getContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    sendRequestToServer();
                } else {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(button, "Check Your Connection", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void sendRequestToServer() {
            RegistrationApi  api= ServiceGenerator.createService(RegistrationApi.class);
            Call<ProfileDao>call=api.toUpdateUserDetails(UserId,ProfileName,Email);
            call.enqueue(new Callback<ProfileDao>() {
                @Override
                public void onResponse(Call<ProfileDao> call, Response<ProfileDao> response) {
                    if (response.isSuccessful()) {
                        dao = response.body();
                        if (dao.getResponse().equalsIgnoreCase("Success")) {
                            progressBar.setVisibility(View.GONE);
                            ProfileName = dao.getData().getData().getName();
                            Email = dao.getData().getData().getEmail();
                            profileEdtv.setText(ProfileName);
                            emailEdtv.setText(Email);
                            mobileEdtv.setText(Mobile);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Snackbar.make(button, "No data found"+dao.getResponse(), Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Snackbar.make(button, "no data found"+response.body(), Snackbar.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProfileDao> call, Throwable t) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        Snackbar.make(button,"no data found", Snackbar.LENGTH_SHORT).show();
                        Log.d("message", "" + t.getMessage());
                        System.out.println("onFAilureExecute" + t.getMessage());
                        if (t != null)
                            t.printStackTrace();
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }

                }
            });
        }
    }
