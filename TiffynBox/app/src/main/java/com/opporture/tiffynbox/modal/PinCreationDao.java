package com.opporture.tiffynbox.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PinCreationDao {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("AddData")
    @Expose
    private AddPinData addData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public AddPinData getAddData() {
        return addData;
    }

    public void setAddData(AddPinData addData) {
        this.addData = addData;
    }

}
