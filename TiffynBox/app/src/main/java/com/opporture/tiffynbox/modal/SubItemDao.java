package com.opporture.tiffynbox.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubItemDao {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("Data")
    @Expose
    private SubItemData data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public SubItemData getData() {
        return data;
    }

    public void setData(SubItemData data) {
        this.data = data;
}
}
