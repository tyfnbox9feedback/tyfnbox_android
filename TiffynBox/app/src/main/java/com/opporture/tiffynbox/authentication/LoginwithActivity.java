package com.opporture.tiffynbox.authentication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.SyncStateContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.opporture.tiffynbox.R;
import com.opporture.tiffynbox.dashboard.DashboardActivity;
import com.opporture.tiffynbox.modal.LoginDao;
import com.opporture.tiffynbox.modal.PinCreationDao;
import com.opporture.tiffynbox.services.RegistrationApi;
import com.opporture.tiffynbox.services.ServiceGenerator;
import com.opporture.tiffynbox.utils.Constants;
import com.opporture.tiffynbox.utils.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginwithActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {
    EditText setpinOne, setpinTwo, setpinThree, setpinFour;
    String Pinone, Pintwo, PinThree, PinFour, Pin, Mobile, UserID;
    ProgressBar progressBar;
    Button continueButton;
    LoginDao loginDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_loginwith);
        Intent intent = getIntent();
        Mobile = intent.getStringExtra("mobile");
        initViews();
        initListeners();
        setWatcherListener();
    }


    private void initListeners() {
        continueButton.setOnClickListener(this);
    }

    private void initViews() {
        setpinOne = findViewById(R.id.etPin1);
        setpinTwo = findViewById(R.id.etPin2);
        setpinThree = findViewById(R.id.etPin3);
        setpinFour = findViewById(R.id.etPin4);
        progressBar = findViewById(R.id.progressbar);
        continueButton = findViewById(R.id.continueButton);
    }

    @Override
    public void onClick(View view) {
        if (view == continueButton) {
            InputMethodManager hide = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            hide.hideSoftInputFromWindow(continueButton.getWindowToken(),
                    InputMethodManager.RESULT_UNCHANGED_SHOWN);
            validateForm();
        }
    }

    private void validateForm() {
        Pinone = setpinOne.getText().toString().trim();
        Pintwo = setpinTwo.getText().toString().trim();
        PinThree = setpinThree.getText().toString().trim();
        PinFour = setpinFour.getText().toString().trim();

        Pin = Pinone + Pintwo + PinThree + PinFour;
        if (Pin.length() == 4 || !Pin.isEmpty()) {
            Log.e("pin", "" + Pin);
            if (Util.isNetworkAvailable(this)) {
                progressBar.setVisibility(View.VISIBLE);
                sendRequestToServer();
            } else {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(continueButton, "Check Your Connection", Snackbar.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "check your pin once", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendRequestToServer() {
        RegistrationApi api = ServiceGenerator.createService(RegistrationApi.class);
        Call<LoginDao> call = api.toUserLogin(Mobile, Pin);
        call.enqueue(new Callback<LoginDao>() {
            @Override
            public void onResponse(Call<LoginDao> call, Response<LoginDao> response) {
                if (response.isSuccessful()) {
                    loginDao = response.body();
                    if (loginDao.getResponse().equalsIgnoreCase("Success")) {
                        Util.saveBooleanInSP(LoginwithActivity.this, Constants.IS_LOGIN, true);
                        if (UserID != null) {
                            Util.saveStringInSP(LoginwithActivity.this, "user_id", loginDao.getData().getUserId());
                        }
                        if (Mobile != null) {
                            Util.saveStringInSP(LoginwithActivity.this, "mobile", loginDao.getData().getUsername());
                        }
                        progressBar.setVisibility(View.GONE);
                        startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Snackbar.make(continueButton, "" + loginDao.getResponse(), Snackbar.LENGTH_SHORT).show();

                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(continueButton, "" + response.message(), Snackbar.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LoginDao> call, Throwable t) {
                try {
                    progressBar.setVisibility(View.GONE);
                    Snackbar.make(continueButton, "" + t.getMessage(), Snackbar.LENGTH_SHORT).show();
                    Log.d("message", "" + t.getMessage());
                    System.out.println("onFAilureExecute" + t.getMessage());
                    if (t != null)
                        t.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }

            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        switchToNextBox();
    }

    // This method is used to switch to next opt box
    public void switchToNextBox() {
        if (setpinOne.getText().toString().length() == 1 && setpinTwo.getText().toString().length() != 1 && setpinThree.getText().toString().length() != 1 && setpinFour.getText().toString().length() != 1) {
            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    setpinTwo.requestFocus();
                }
            }, 50);
        } else if (setpinOne.getText().toString().length() == 1 && setpinTwo.getText().toString().length() == 1 && setpinThree.getText().toString().length() != 1 && setpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setpinThree.requestFocus();
                }
            }, 50);
        } else if (setpinOne.getText().toString().length() == 1 && setpinTwo.getText().toString().length() == 1 && setpinThree.getText().toString().length() == 1 && setpinFour.getText().toString().length() != 1) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setpinFour.requestFocus();
                }
            }, 50);
        }
    }


    // This is used to set TextWatcher Listener
    public void setWatcherListener() {
        setpinOne.addTextChangedListener(this);
        setpinTwo.addTextChangedListener(this);
        setpinThree.addTextChangedListener(this);
        setpinFour.addTextChangedListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        handleBackPressFocus();
        return super.onKeyDown(keyCode, event);
    }

    // handle focus when user press back key of soft keyboard
    private void handleBackPressFocus() {
        if (setpinFour.hasFocus()) {
            setpinThree.requestFocus();
           setpinThree.setSelection(setpinThree.getText().length());
        } else if (setpinThree.hasFocus()) {
            setpinTwo.requestFocus();
            setpinTwo.setSelection(setpinTwo.getText().length());
        } else if (setpinTwo.hasFocus()) {
            setpinOne.requestFocus();
            setpinOne.setSelection(setpinOne.getText().length());
        }
    }
}