package com.opporture.tiffynbox.utils;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.regex.Pattern;


public class Util {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static void saveStringInSP(Context paramContext, String paramString1, String paramString2)
    {
        System.out.println("saveStringInSP "+paramString1+" "+paramString2);
        SharedPreferences.Editor sharedPreferenceEditor = paramContext.getSharedPreferences(paramString1, Context.MODE_PRIVATE).edit();
        System.out.println("sharedPreferenceEditor object "+sharedPreferenceEditor.toString());
        sharedPreferenceEditor.putString(paramString1, paramString2);
        sharedPreferenceEditor.commit();
    }
    public static String getStringFromSP(Context paramContext, String paramString)
    {
        System.out.println("getStringFromSP "+paramString);
        SharedPreferences sharedPreferences  = paramContext.getSharedPreferences(paramString, Context.MODE_PRIVATE);
        System.out.println("sharedPreferences "+sharedPreferences.toString());
        if (sharedPreferences.contains(paramString)) {
            return sharedPreferences.getString(paramString, "");
        }
        return "";
    }
    public static void saveBooleanInSP(Activity paramActivity, String paramString, boolean paramBoolean)
    {
        System.out.println("saveBooleanInSP "+paramString+" "+paramBoolean);
        SharedPreferences.Editor sharedPreferencesEditor = paramActivity.getSharedPreferences(paramString, Context.MODE_PRIVATE).edit();
        System.out.println("sharedPreferenceEditor object "+sharedPreferencesEditor.toString());
        sharedPreferencesEditor.putBoolean(paramString, paramBoolean);
        sharedPreferencesEditor.commit();
    }
    public static boolean getBooleanFromSP(Context paramActivity, String paramString)
    {
        System.out.println("getBooleanFromSP"+paramString);
        boolean bool = false;
        SharedPreferences sharedPreferences = paramActivity.getSharedPreferences(paramString, Context.MODE_PRIVATE);
        System.out.println("sharedPreferences "+sharedPreferences.toString());
        if (sharedPreferences.contains(paramString)) {
            bool = sharedPreferences.getBoolean(paramString, false);
        }
        return bool;
    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
   //validate phone number
    public static boolean isValidPhoneNumber(String phoneNumber) {
        return phoneNumber.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$");
    }

    //validate email id
    public static boolean isValidEmaillId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
