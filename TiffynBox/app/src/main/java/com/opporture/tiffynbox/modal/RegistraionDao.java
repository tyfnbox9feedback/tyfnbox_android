package com.opporture.tiffynbox.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegistraionDao {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("UserData")
    @Expose
    private AddData userData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public AddData getUserData() {
        return userData;
    }

    public void setUserData(AddData userData) {
        this.userData = userData;
    }

}